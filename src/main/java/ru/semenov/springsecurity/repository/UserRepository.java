package ru.semenov.springsecurity.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.semenov.springsecurity.models.MyUser;

import java.util.Optional;

public interface UserRepository extends JpaRepository<MyUser,Long> {
    Optional<MyUser> findByName(String username);

}
