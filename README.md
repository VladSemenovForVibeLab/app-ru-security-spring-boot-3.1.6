# Учетная система для приложения Spring Boot

## Описание проекта
Учетная система для приложения Spring Boot с использованием Spring Security для базовой аутентификации пользователей. Это приложение выполняет роль учета приложений и позволяет управлять пользователями.

## Требования
- Java 17
- Maven
- Spring Boot 3.1.2
- Spring Security
- JavaFaker
- Spring Data JPA
- Hibernate
- Lombok
- PostgreSQL

## Установка и настройка
1. Клонируйте репозиторий на локальную машину.
2. Убедитесь, что у вас установлена Java 17 и Maven.
3. Создайте базу данных PostgreSQL.
4. Откройте файл application.properties и настройте следующие параметры:

 ```
   spring.datasource.url=jdbc:postgresql://localhost:5432/название_базы_данных
   spring.datasource.username=ваше_имя_пользователя_postgresql
   spring.datasource.password=ваш_пароль_postgresql
 ```

5. Соберите проект с помощью команды:

```
   mvn clean install
```

## Запуск приложения
1. Выполните команду:

```
   mvn spring-boot:run
```

2. Приложение будет запущено на порту 8082.

## API

### 1. Получение списка приложений
```
GET /api/v1/apps/all=app
```
Возвращает массив объектов, представляющих список приложений.

Пример ответа:
```
[
{
"id": 1,
"name": "Приложение 1",
"author": "Автор 1",
"version": "1.0"
},
{
"id": 2,
"name": "Приложение 2",
"author": "Автор 2",
"version": "2.0"
},
...
]
```

## Дополнительная информация
- Документация Spring Boot (https://docs.spring.io/spring-boot/docs/3.1.2/reference/htmlsingle/)
- Документация Spring Security (https://docs.spring.io/spring-security/site/docs/5.6.1/reference/html5/)
- Документация Spring Data JPA (https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#reference)
- Документация Hibernate (https://docs.jboss.org/hibernate/orm/current/userguide/html_single/Hibernate_User_Guide.html)
- Документация Lombok (https://projectlombok.org/features/all)

Приятного использования!